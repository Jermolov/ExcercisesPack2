package ee.bcs.valiit;

public class Main {
    //Ulesande 2 - String Processing

    public static void main(String[] args) {
        System.out.println("Hello, World!");
        System.out.println("Hello \"World\"");
        System.out.println("Steven Hawking once said:\"Life would be tragic if it weren't funny\"");
        System.out.println("See on teksti esimene pool "+"See on teksti teine pool");
        System.out.println("Elu on ilus");
        System.out.println("Elu on 'ilus'");
        System.out.println("Elu on \"ilus\"");
        System.out.println("Koige rohkem segadust tekitab\"-margi kasutamine sone sees.\"");
        System.out.println("'Kolm' -kolm, ' neli' -neli, \"viis\" -viis.");
    }
}
